/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;

/**
 *
 * @author teemu
 */
public class TilaustenKäsittelyEasymockTest {

    @Test
    public void testaaKäsittelijäWithEasyMockHinnoittelija_HintaAlle100() {
        // arrange
        float alkuSaldo = 100.0f;
        float listaHinta = 99.0f;
        float alennus = 20.0f;
        float loppuSaldo = alkuSaldo - (listaHinta * (1 - alennus / 100));
        Asiakas asiakas = new Asiakas(alkuSaldo);
        Tuote tuote = new Tuote("TDD in Action", listaHinta);
        Hinnoittelija_IF hinnoittelija
                = EasyMock.createMock(Hinnoittelija_IF.class);
        // record

        hinnoittelija.aloita();

        EasyMock.expect(hinnoittelija.getAlennusProsentti(asiakas, tuote))
                .andReturn(alennus);

        EasyMock.expect(hinnoittelija.getAlennusProsentti(asiakas, tuote))
                .andReturn(alennus);

        hinnoittelija.lopeta();

        EasyMock.replay(hinnoittelija);

        // act
        TilaustenKäsittely käsittelijä = new TilaustenKäsittely();
        käsittelijä.setHinnoittelija(hinnoittelija);
        käsittelijä.käsittele(new Tilaus(asiakas, tuote));
        // assert
        assertEquals(loppuSaldo, asiakas.getSaldo(), 0.001);
        EasyMock.verify(hinnoittelija);
    }

    @Test
    public void testaaKäsittelijäWithEasyMockHinnoittelija_Hintatasan100() {
        // arrange
        float alkuSaldo = 200.0f;
        float listaHinta = 100.0f;
        float alennus = 20.0f;
        float loppuSaldo = alkuSaldo - (listaHinta * (1 - alennus / 100));
        Asiakas asiakas = new Asiakas(alkuSaldo);
        Tuote tuote = new Tuote("TDD in Action", listaHinta);
        Hinnoittelija_IF hinnoittelija
                = EasyMock.createMock(Hinnoittelija_IF.class);
        // record

        hinnoittelija.aloita();

        EasyMock.expect(hinnoittelija.getAlennusProsentti(asiakas, tuote))
                .andReturn(alennus);

        hinnoittelija.setAlennusProsentti(asiakas, (alennus + 5));

        EasyMock.expect(hinnoittelija.getAlennusProsentti(asiakas, tuote))
                .andReturn(alennus);

        hinnoittelija.lopeta();
        EasyMock.expectLastCall();

        EasyMock.replay(hinnoittelija);

        // act
        TilaustenKäsittely käsittelijä = new TilaustenKäsittely();
        käsittelijä.setHinnoittelija(hinnoittelija);
        käsittelijä.käsittele(new Tilaus(asiakas, tuote));
        // assert
        assertEquals(loppuSaldo, asiakas.getSaldo(), 0.001);
        EasyMock.verify(hinnoittelija);
    }
}
